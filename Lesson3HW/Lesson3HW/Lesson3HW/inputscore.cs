﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3HW
{
    public static class inputscore
    {

       public static double NhapDiem(string message)
        {
            Console.Write(message);
            double diem = double.Parse(Console.ReadLine());
            return diem;
        }
       public static double TinhDiemTrungBinh(double diemVan, double diemToan, double diemAnh)
        {
            double tongDiem = diemVan + diemToan + diemAnh;
            double diemTrungBinh = tongDiem / 3.0;
            return diemTrungBinh;
        }
       public static void XuatDiem( string ten, double diemTrungBinh)
        {
            Console.WriteLine($"Ten hoc sinh:{ten}, Diem TB: {diemTrungBinh}");
        }
    }

       
}
